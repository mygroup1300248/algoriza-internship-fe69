import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from '../views/LoginView.vue'
import RegisterView from '../views/RegisterView.vue'
import searchResults from '../views/SearchResults.vue'
import HotelDetails from '../views/HotelDetails.vue'
import Checkout from '../views/Checkout.vue'
import MyTrips from '../views/MyTrips.vue'


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: "/login",
      name: "login",
      component: LoginView
    },
    {
      path: "/register",
      name: "register",
      component: RegisterView
    },
    {
      path: "/searchresults",
      name: "searchresults",
      component: searchResults,
    },
    {
      path: "/hoteldetails",
      name: "hoteldetails",
      component: HotelDetails,
    },
    {
      path: "/checkout",
      name: "checkout",
      component: Checkout,
    },
    {
      path: "/mytrips",
      name: "mytrips",
      component: MyTrips,
    },
  ],
});

export default router
