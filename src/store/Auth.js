import { ref } from 'vue';

 const isAuthenticated = ref(localStorage.getItem('authToken') !== null);


export const logout = () => {
  localStorage.removeItem('authToken');
  isAuthenticated.value = false;
  
};