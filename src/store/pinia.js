import { defineStore } from 'pinia';

export const useFormDataStore = defineStore({
  id: 'form-data',
  state: () => ({
    dest: null,
    checkIn: null,
    checkOut: null,
  }),
  actions: {
    setFormData(formData) {
      Object.assign(this, formData);
    },
  },
});