/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html","./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        "BGcolor": "#FFF",
        "BGcovid":"#FCEFCA",
        "font":"#181818",
        "button":"#2F80ED",
        "text":"var(--Gray-2, #4F4F4F)",
        "list":"#1A1A1A",
        "bList":"#E0E0E0",
        "strike":"var(--Red, #EB5757)",
        "off":"var(--Green-1, #219653)",
        "BG":"#F4F4F4",
        "footer":"#EBEBEB",
        "checkout":"#F2C94C",
        "red":"#EB5757",
        "green":"#85E0AB",
        "booking": "rgba(111, 207, 151, 0.20)"
      },
      linearGradientColors: {
        'custom': 'linear-gradient(180deg, rgba(244, 244, 244, 0.00) 0%, #FFF 100%)',
      },
      width: {
        '124': '1240px',
        '103': '1030px',
        '600': '600px',
        '800': '800px',
        '400': '400px'
      },
      height:{
        '280':'280px',
        '530':'530px',
      },
      margin:{
        '500': '500px',
      },
      backgroundImage: {
        'hero': "url('../images/Rectangle 18.png')",
        'search': "linear-gradient(180deg, #2969BF 0%, #144E9D 100%)",
      },
      backgroundColor:{
        'input': "var(--Gray-6, #F2F2F2)",
        'details':"linear-gradient(180deg, rgba(244, 244, 244, 0.00) 0%, #FFF 100%)",
      },
    },
    fontFamily: {
      WorkSans: ["Roboto, sans-serif"],
    },
    container: {
      padding: "2rem",
      center: true,
    },
    screens: {
      sm: "640px",
      md: "768px",
    },
  },
  plugins: [],
};

